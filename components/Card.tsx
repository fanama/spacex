import Router from "next/router";
import * as React from "react";
import { CardProps } from "../domain/props";

import styles from './style.module.scss';

export function Card({ launch }: CardProps) {
 const redirect = () => {
	Router.push(`/launch?id=${launch.id}`);
  };
  return (
    <div className={styles.Card} onClick={redirect} >
      <h3> Launch ID : {launch.mission_name}</h3>
      <h4>{launch.id}</h4>
    </div>
  );
}
