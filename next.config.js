module.exports = {
  swcMinify: false,
  presets: ["next/babel"],
  env: {
    ACCESS_TOKEN_SECRET: "secret",
    PORT: 3004,
    BASE_API: "http://localhost:3001",
    LOGIN_API: "http://localhost:3002",
  },
  async rewrites() {
    return [
      {
        source: "/api/:path*",
        destination: "http://api.spacex.land/rest/:path*",
      },
    ];
  },
};
