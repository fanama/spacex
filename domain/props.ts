import {Launch} from "./launch";

export interface CardProps {
  launch: Launch;
}
