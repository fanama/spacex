export interface Launch {
  id: number;
  mission_name?: string;
  details?: string;
  launch_date_local?: string;
  launch_site?: Site;
  rocket?: Rocket;
}

interface Site {
  site_name_long: string;
  site_name: string;
}

interface Rocket {
  first_stage: Stage;
}

interface Stage {
  cores: Core[];
}

interface Core {
  block?: number;
  core?: {missions: Mission[]};
}

export interface Mission {
  flight: number;
  name: string;
}
